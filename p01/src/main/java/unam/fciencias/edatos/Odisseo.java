package unam.fciencias.edatos;

import java.util.ArrayList;
import java.util.Collection;

import javafx.application.Application;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.shape.Line;
import unam.fciencias.edatos.pruebas.Muestra;

/**
 * Hello world!
 *
 */
public class Odisseo extends Application {

    @Override
    public void start(Stage stage) {

        Collection<Muestra> muestras = new ArrayList<>();
        Muestra ariel = new Muestra(2,100000);
        Muestra atza = new Muestra(5,100000);
        Muestra demian = new Muestra(10,100000);
        Muestra armando = new Muestra(100,100000);
        Muestra merino = new Muestra(1000,100000);
        Muestra onirem = new Muestra(2000,100000);
        ariel.generarPrueba();
        atza.generarPrueba();
        demian.generarPrueba();
        merino.generarPrueba();
        onirem.generarPrueba();
        armando.generarPrueba();
        muestras.add(ariel);
        muestras.add(atza);
        muestras.add(demian);
        muestras.add(armando);
        muestras.add(merino);
        muestras.add(onirem);
        System.out.println("Resultados: "+ muestras  );

        Circle puntoAtza = new Circle(50, 500-atza.getResultado(), 7);
        Circle puntoAriel = new Circle(100, 500-ariel.getResultado(), 7);
        Circle puntoDemian = new Circle(200, 500-demian.getResultado(), 7);
        Circle puntoArmando = new Circle(280, 500-armando.getResultado(), 7);
        Circle puntoMerino = new Circle(380, 500-merino.getResultado(), 7);
        Circle puntoOnirem = new Circle(500, 500-onirem.getResultado(), 7);
        puntoOnirem.setFill(Color.valueOf("3149FA"));
        puntoMerino.setFill(Color.valueOf("FA6093"));
        puntoAriel.setFill(Color.valueOf("DB66FA"));
        puntoAtza.setFill(Color.valueOf("FAF005"));
        puntoArmando.setFill(Color.valueOf("129A05"));
        puntoDemian.setFill(Color.rgb(235, 64, 52));

        Line segmentoInicioAtza = new Line(0,500,50,500-atza.getResultado());
        Line segmentoAtzaAriel = new Line(50,500-atza.getResultado(),100,500-ariel.getResultado());
        Line segmentoArielDemian = new Line(100,500-ariel.getResultado(),200,500-demian.getResultado());
        Line segmentoDemianArmando = new Line(200,500-demian.getResultado(),280,500-armando.getResultado());
        Line segmentoArmandoMerino = new Line(280,500-armando.getResultado(),380,500-merino.getResultado());
        Line segmentoMerinoOnirem = new Line(380,500-merino.getResultado(),500,500-onirem.getResultado());
        segmentoInicioAtza.setStroke(Color.valueOf("0E0796"));
        segmentoInicioAtza.setStrokeWidth(3);
        segmentoAtzaAriel.setStroke(Color.valueOf("0E0796"));
        segmentoAtzaAriel.setStrokeWidth(3);
        segmentoArielDemian.setStroke(Color.valueOf("0E0796"));
        segmentoArielDemian.setStrokeWidth(3);
        segmentoDemianArmando.setStroke(Color.valueOf("0E0796"));
        segmentoDemianArmando.setStrokeWidth(3);
        segmentoArmandoMerino.setStroke(Color.valueOf("0E0796"));
        segmentoArmandoMerino.setStrokeWidth(3);
        segmentoMerinoOnirem.setStroke(Color.valueOf("0E0796"));
        segmentoMerinoOnirem.setStrokeWidth(3);
        Collection<Node> nodos = new ArrayList<Node>();
        nodos.add(segmentoInicioAtza);
        nodos.add(segmentoAtzaAriel);
        nodos.add(segmentoArielDemian);
        nodos.add(segmentoDemianArmando);
        nodos.add(segmentoMerinoOnirem);
        nodos.add(segmentoArmandoMerino);

        nodos.add(puntoAriel);
        nodos.add(puntoArmando);
        nodos.add(puntoAtza);
        nodos.add(puntoDemian);
        nodos.add(puntoMerino);
        nodos.add(puntoOnirem);


        Group root = new Group(nodos);
        Scene scene = new Scene(root, 900, 900);

        stage.setTitle("Primer programa en JavaFX");
        stage.setScene(scene);
        stage.show();
    }


    public static void main( String[] args )
    {
        launch();
    }
}
