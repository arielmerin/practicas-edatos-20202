package unam.fciencias.edatos;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple Odisseo.
 */
public class OdisseoTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public OdisseoTest(String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( OdisseoTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
